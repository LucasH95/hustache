Hustache Lucas
Jasser Baali
14/10/21

1) **Quel est le masque de sous-réseau de chaque machine ?**

M1 : 255.255.0.0

CDR/16 : 11111111.11111111.00000000.00000000

M2 : 255.255.0.0

CDR/16 : 11111111.11111111.00000000.00000000

M3 : 255.255.0.0

CDR/16 : 11111111.11111111.00000000.00000000

2) **A quel sous-réseau IP appartient chaque machine ? Justifier.**
Leurs adresses réseau est : 192         168         0          10
masque de sous réseau : 1111 1111   1111 1111   0000 0000   0000 0000
adresse réseau :        1111 1111   1111 1111   0000 0000   0000 0000 = 192.168.0.0/16
 adresse broadcast :    1111 1111   1111 1111   1111 1111  1111 1111 = 192.168.255.255/16

Leurs adresses réseau est : 192         168         10          10
masque de sous réseau : 1111 1111   1111 1111   0000 0000   0000 0000
adresse réseau :        1111 1111   1111 1111   0000 0000   0000 0000 = 192.168.0.0/16
adresse broadcast :     1111 1111   1111 1111   1111 1111  1111 1111  = 192.168.255.255/16
			

3) **Peuvent-elles potentiellement communiquer entre elles ? Justifier.**

Oui elle peuvent communiquer entre elles car elles ont le même masque de sous réseau et d'adresse réseau.

4) **Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.**

Non car deux ordinateurs ont la même adresse ip (PC1 et PC2)

5) **Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA ?**

La commande est : ipconfig

6) **Quelle commande permet de tester que les messages IP sont bien transmis entre deux machines ?**

La commande est : ping 192.168.10.10


