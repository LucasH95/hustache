%Mission2 filius 
%Lucas Hustache / Sousa Fernandes Ricardo
%21/10/2021

#1.Quel est le CIDR de chaque machine ? Justifier.
Le cidr de chaque machine est de /16 puisque le masque reseau est "255.255.0.0"

#2.A quel sous-réseau IP appartient chacune de ces 4 machines ? Justifier.

*@ip M1  : 10      .10      .1       .100     /16*
*masque  : 11111111.11111111.00000000.00000000*
*@reseau : 10      .10      .0       .0* 

*@ip M2  : 10      .0       .1       .255     /16*
*masque  : 11111111.11111111.00000000.00000000*
*@reseau : 10      .0     .0       .0* 

*@ip M3  : 10      .0       .10       .245    /16*
*masque  : 11111111.11111111.00000000.00000000*
*@reseau : 10      .0       .0       .0* 

*@ip M4  : 10      .10      .20      .128     /16*
*masque  : 11111111.11111111.00000000.00000000*
*@reseau : 10      .10      .0       .0* 


#3.Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés ?

*=2^(32-CIDR)-2*
*=2^(32-16)-2*
*=2^(16)-2*
*=65534*

#4.Quelles machines peuvent potentiellement communiquer entre elles ? Justifier.

*M2 <=> M3 : M2 et M3 peuvent communiquer dans les deux sens puisqu'elles sont sous le même reseau*

*M1 <=> M4 : M1 et M4 peuvent communiquer dans les deux sens puisqu'elles sont sous le même reseau*

#5.Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.	

*M1 peut communiquer avec M4 tester grace à la commande "/> ping 10.10.20.128 "*

*M4 peut communiquer avec M1 tester grace à la commande "/> ping 10.10.1.100 "*

*M2 peut communiquer avec M3 tester grace à la commande "/> ping 10.0.10.245 "*

*M3 peut communiquer avec M2 tester grace à la commande "/> ping 10.0.1.255 "*

*Les combinaison non cite ne peuvent pas communiquer entre elles*


#6.Quelle est la commande complète qui permet à M1 de tester la liaison avec M2

*La commande permetant à M1 de tester sa liaison avec M2 et "/> ping 10.0.1.255 "*

#ETAPE 2

#1.Quelles sont les adresses IP possibles pour M5 et M6 ?

*M5 devra avoir une @ip dans le reseau similaire a M1 donc "10.10.0.0"*

*M6 devra avoir une @ip dans le reseau similaire a M2 donc "10.0.0.0"*

#2.Combien de machines peut-on encore ajouter dans chaque sous-réseau ?

*=2^(32-CIDR)-2*
*=2^(32-16)-2*
*=2^(16)-2*
*=65534*

#3.Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser ?

*Si M6 voudrais envoyer un message à toutes les machines de son sous-réseau , M6 devra utiliser l'adresse broadcast (diffusion)*

#4.Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages ?

*Pour permettre à toutes les machine d'échanger des messages il faudrais ajouter un routeur*

#ETAPE 3

#1.Expliquer brièvement le fonctionnement d’un routeur.

*Un routeur sert à connecter un modem a plusieurs appareils , permetent la communication entre ces appareils et internet . ils se connectent physiquement via un câble aux appareils à brancher à internet.*

#2.Compléter la configuration physique pour permettre aux différentes machines des différents réseaux logiques de communiquer.

#2.1.Combien d’interfaces réseaux sont nécessaires ?

*Il y a deux reseaux faudrais donc 2 interfaces réseaux*

#2.2.Quelle adresse IP aura chaque interface ?

*Les interfaces auront @ip de 10.10.255.254 et 10.0.255.254*

#3.Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.

*Oui 

#4.Quelle nouvelle configuration est nécessaire pour permettre aux machines de communiquer avec des machines appartenant à d’autres réseaux (M5 avec M6 par exemple)?



