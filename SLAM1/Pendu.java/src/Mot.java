public class Mot {
    //atribut contenu de type string
    private String contenu; // non attribuer donc nul

    //constructeur sans arguments
    public Mot() {
        //initialisation de contenu
        this.contenu="";
    }
    public Mot(String contenu){
        this.contenu=contenu;
    }
    
    //getter (recuper une info)
    public String getContenu(){
        //retourner le contenu de la variable de l'attribut contenu
        return this.contenu;

    }
    //setter (modifier une info d'un objet)
    public void setContenu(String contenu){
        this.contenu=contenu;
    }
    public void afficher(){
        System.out.println("contenu du mot :" + this.contenu);
    }
    //cette methode vrai si la lettre est contenu dans le mot ou faux si celle ci ne l'est pas 
    public boolean contient(char lettre){
        //char ne compte que un character initialise par les quote
        return true;

    }

}