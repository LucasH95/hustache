import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

// la classe helloworld est une application
// cet heritage est primordial
public class HelloWorld extends Application {

// pour demarrer une application public static void main
    public static void main(String[] args) {
// une methode de helloworld heritée de application
        launch(args);
    }

// methode start de la classe helloworld heritée d'application
// elle sert a initialiser l'environnement graphique (fenetre et composant graphique)
    
    @Override
    public void start(Stage primaryStage) {
// ajoute un titre a primary.stage
        primaryStage.setTitle("Hello World!");
// creation d'un objet button
        Button btn = new Button();
// changement du texte du bouton btn
        btn.setText("Say 'Hello World'");
// gestion de l'envent action sur bouton (avec entrer ou clique de souris)
        btn.setOnAction(new EventHandler<ActionEvent>() {
// implementer la methode handle pour decrire la reaction apres l'action sur btn
            @Override
            public void handle(ActionEvent event) {

                System.out.println("Hello World!");
                primaryStage.setTitle("Bonjour");
            }
        });
// root est un panel qui recoit et positionne les élément graphique les uns apres les autres
        StackPane root = new StackPane();
// gestion de style comme dans les css
        root.setStyle("-fx-background-color: green");
// ajout dans le panel du bouton btn
        root.getChildren().add(btn);
        
// creation d'une scene avec panel root(racine), hauteur longueur en pixels
        Scene scene1=new Scene(root, 300, 250);
// j'associe a la fenetre la scene crée
        primaryStage.setScene(scene1);
        primaryStage.show();
    }
}