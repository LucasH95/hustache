-- Adminer 4.8.1 MySQL 5.7.36 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `easyline`;
CREATE DATABASE `easyline` /*!40100 DEFAULT CHARACTER SET utf16 */;
USE `easyline`;

DROP TABLE IF EXISTS `adressepostales`;
CREATE TABLE `adressepostales` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `numVoie` varchar(100) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `codepostale` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `adressepostales` (`id`, `numVoie`, `ville`, `codepostale`) VALUES
(1,	'2 rue gabriel faure',	'JLM',	'95280');

DROP TABLE IF EXISTS `bagages`;
CREATE TABLE `bagages` (
  `numero` int(7) NOT NULL,
  `couleur` varchar(20) NOT NULL,
  `poids` int(3) NOT NULL,
  `voyageur` int(3) NOT NULL,
  PRIMARY KEY (`numero`),
  KEY `voyageur` (`voyageur`),
  CONSTRAINT `bagages_ibfk_1` FOREIGN KEY (`voyageur`) REFERENCES `voyageur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;


DROP TABLE IF EXISTS `voyageur`;
CREATE TABLE `voyageur` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `anneeNaiss` int(4) NOT NULL,
  `adresse` int(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adresse` (`adresse`),
  CONSTRAINT `voyageur_ibfk_1` FOREIGN KEY (`adresse`) REFERENCES `adressepostales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `voyageur` (`id`, `nom`, `prenom`, `anneeNaiss`, `adresse`) VALUES
(1,	'Hustache',	'Lucas',	2003,	1),
(2,	'Palpatine',	'Christophe',	2001,	1);

-- 2022-11-10 09:10:23
