/* Lucas Hustache
BTS2
22/09/2022 */


public class Bagage{

    private static int numero;
    private static String couleur;
    private static int poids; 

            public Bagage (){

            }

            public Bagage(int numero,String couleur,int poids){
                this.numero=numero;
                this.couleur=couleur;
                this.poids=poids;
            }
            public void setNumero(int numeroV){
                numero = numeroV;
            } 
            public void setCouleur(String couleurV){
                couleur = couleurV;
            }       
            public void setPoids(int poidsV){
                poids = poidsV;
            }
            public static int getNumero()
            {
            return numero;
            } 
            public String getCouleur()
            {
            return couleur;
            } 
            public int getPoids()
            {
            return poids;
            }
            public void afficher(){
                System.out.println(this.numero);
                System.out.println(this.couleur);
                System.out.println(this.poids);
            }

    }