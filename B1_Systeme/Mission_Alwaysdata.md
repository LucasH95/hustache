﻿Travail alwaysdata :

Hustache

Lucas

BTS1

19/04/22

Etape 0 :

\1) Quels services sont offerts par Alwaysdata ?

` `Les services offert par AlwaysData sont :

- accès SSH complet
- ` `IPv6 natif et généralisé
- ` `support de nombreuses technologies (PHP, Node.js, Python, Ruby, MariaDB, PostgreSQL, RabbitMQ, etc.)
- SSL: configuration automatique et gratuite via Let’s Encrypt
- ` `paramétrage avancé (configuration Apache, php.ini modifiable, installation locale de programmes et bibliothèques, etc.)
- ` `sauvegarde quotidienne avec conservation pendant 30 jours
- ` `Emails, mailings-lists, redirections,anti-spam
- tâches planifiées (crons) et services monitorés
- ` `statistiques web automatiques via Matomo
- ` `accès distant aux bases de données autorisé
- API REST
- cache HTTP et WAF intégrés

2) Quels services seront nécessaires pour déployer un site web (HTML, CSS, JS et PHP) ?

Il faudrait l'héberger afin de déployer un site web (avoir un hébergeur).

3) Quel est le nom de domaine choisi pour le déploiement de votre site ?

[www.hustache.com](http://www.hustache.com/)



Etape 1 :

1) Expliquer l'intérêt du protocole SSH. Sur quel port est-il actif par défaut ?

SSH est un protocole d’administration à distance qui permet aux utilisateurs de contrôler et de modifier leurs serveurs distants sur Internet.Il utilise des techniques cryptographiques pour s’assurer que toutes les communications vers et depuis le serveur distant se produisent de manière chiffrée.Il fournit un mécanisme pour authentifier un utilisateur distant, transférer les entrées du client vers l’hôte et relayer la sortie vers le client.

Il est sur le port 22.

2) Quel autre protocole semble avoir les mêmes fonctionnalités ? Que fait SSH qui n'est pas possible avec le 2e ?

Le protocole FTP a les mêmes fonctionnalités que SSH le seul truc qui change c'est que SSH permet de créer un dialogue entre deux pc tant qu'ils ont un chemin électrique entre eux pour le transport des communications.

3) Activer un accès au serveur via ce protocole. Quelles étapes sont nécessaires ?

Les étapes nécessaires sont de créer un mot de passe sur Alwaysdata afin de pouvoir se connecter au ssh.

4) Se connecter à votre espace dédié sur le serveur via ce protocole. Quelle est la ligne de commande nécessaire pour y arriver ?

ssh <hustache@ssh-hustache.alwaysdata.net>

5) Dans quel repertoire faut-il déposer vos fichiers du site si vous voulez le voir en ligne ? (chemin complet sur le serveur)





