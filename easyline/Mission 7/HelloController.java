package fr.hautil.mission7;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Bienvenue sur JavaFX!");
        System.out.println("Je viens d'appuyer sur le bouton !");
    }


    }

