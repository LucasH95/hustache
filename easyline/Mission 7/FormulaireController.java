package fr.hautil.mission7;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


public class FormulaireController {

    @FXML
    TextField nom;
    @FXML
    TextField prenom;
    @FXML
    TextField AnneeNaiss;
    @FXML
    TextField Adresse;
    @FXML
    TextField Ville;
    @FXML
    TextField CodePostal;

    @FXML
    public void annulerForm(ActionEvent actionEvent) {
        nom.setText("");
        prenom.setText("");
        AnneeNaiss.setText("");
        Adresse.setText("");
        Ville.setText("");
        CodePostal.setText("");
    }
    @FXML
    public void ajouterForm(ActionEvent actionEvent) {
        System.out.println(nom.getText());
        System.out.println(prenom.getText());
        System.out.println(AnneeNaiss.getText());
        System.out.println(Adresse.getText());
        System.out.println(Ville.getText());
        System.out.println(CodePostal.getText());
    }
}
