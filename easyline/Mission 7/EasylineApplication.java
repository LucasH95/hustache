package fr.hautil.mission7;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class EasylineApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(EasylineApplication.class.getResource("Formulaire.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 600);//Changement de taille
        stage.setTitle("Formulaire Easyline");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {//launch();
        String nomV="nomTest";//position de la requete select
        String prenomV="prenomTest";
        int anneeNaiss=2033;
    }

    public static void insertVoyageur(Voyageur v) {

        Connection connection = null;
        String url = "jdbc:mariadb://localhost:3306/easyline";
        String user = "lucas";
        String pwd = "K5b889e3";
        String reqAdresse = "INSERT INTO AdressePostale (numvoie, ville, codepostal)" +
                "VALUES (?,?,?)";
        String reqVoyageur = "INSERT INTO voyageurs (nom, prenom, Age,adresse)" +
                "VALUES (?,?,?,?);";
        int idAdresse = 3;
        v = null;

        try {
            connection = DriverManager.getConnection(url, user, pwd);
            System.out.println("Vous êtes bien connecté");
            PreparedStatement pstmt = connection.prepareStatement(reqVoyageur);
            pstmt.setString(1, v.getNom());
            pstmt.setString(2, v.getPrenom());
            pstmt.setInt(3, v.getAge());
            pstmt.setInt(4, idAdresse);

        } catch (SQLException e) {
            System.out.println("Problème");
            e.printStackTrace();
        } finally {
            if (connection != null) ;
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                //throw new RuntimeException(e);
            }

        }
    }
}
