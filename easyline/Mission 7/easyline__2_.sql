-- Adminer 4.8.1 MySQL 8.0.31 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `adressepostales`;
CREATE TABLE `adressepostales` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numVoie` varchar(100) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `codepostale` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `adressepostales` (`id`, `numVoie`, `ville`, `codepostale`) VALUES
(1,	'2 rue gabriel faure',	'JLM',	'95270'),
(2,	'2 rue Gabriel Fauré',	'JLM',	'95270'),
(3,	'42 avenue du général de Fourier',	'Mantes',	'78000');

DROP TABLE IF EXISTS `bagages`;
CREATE TABLE `bagages` (
  `numero` int NOT NULL,
  `couleur` varchar(20) NOT NULL,
  `poids` int NOT NULL,
  `voyageur` int NOT NULL,
  PRIMARY KEY (`numero`),
  KEY `voyageur` (`voyageur`),
  CONSTRAINT `bagages_ibfk_1` FOREIGN KEY (`voyageur`) REFERENCES `voyageurs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `bagages` (`numero`, `couleur`, `poids`, `voyageur`) VALUES
(123,	'rouge',	23,	1);

DROP TABLE IF EXISTS `voyageurs`;
CREATE TABLE `voyageurs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `anneeNaiss` int NOT NULL,
  `adresse` int NOT NULL,
  `type` enum('normal','privilege','handicape') NOT NULL DEFAULT 'normal',
  PRIMARY KEY (`id`),
  KEY `adresse` (`adresse`),
  CONSTRAINT `voyageurs_ibfk_2` FOREIGN KEY (`adresse`) REFERENCES `adressepostales` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `voyageurs` (`id`, `nom`, `prenom`, `anneeNaiss`, `adresse`, `type`) VALUES
(1,	'Abdelmoula',	'Zoubeïda',	1977,	1,	'normal'),
(2,	'Abdelmoula',	'Lina',	2002,	1,	'normal'),
(3,	'Abdelmoula',	'Zied',	2005,	1,	'normal'),
(4,	'Merlin',	'Bienvenu',	1955,	3,	'normal');

-- 2022-12-15 08:59:34
