//Lucas Hustache
//BTS 2
// 08/09/2022

public class Compagnie {
    
    public String nom;
    public int code;
    public double ca;
    public String couleurPrincipaleLogo;
    public String couleurSecondaireleLogo; // couleur secondaire 
 
    public Compagnie(){
    }
    public Compagnie(String nomCompagnie , int codeCompagnie , String couleurCompagnie){
        this.nom=nomCompagnie; //  nom de compagnie
        this.code=codeCompagnie; //  code
        this.couleurPrincipaleLogo=couleurCompagnie; // couleur principal

    }
    public Compagnie(String nomCompagnie ,String couleurCompagnie, String couleurSecondaireleLogo  ){
        this.nom=nomCompagnie; // nom de compagnie
        this.couleurPrincipaleLogo=couleurCompagnie; // couleur principal
        this.couleurSecondaireleLogo=couleurSecondaireleLogo;
    }
    public Compagnie(String nomCompagnie ,int codeCompagnie ,String couleurCompagnie, String couleurSecondaireleLogo  ){
        this.nom=nomCompagnie; // nom de compagnie
        this.code=codeCompagnie;  // code
        this.couleurPrincipaleLogo=couleurCompagnie; // couleur principal
        this.couleurSecondaireleLogo=couleurSecondaireleLogo; // couleur secondaire
    }
    public void afficher(){
        System.out.println(this.nom);  //afficher nom
        System.out.println(this.code); // afficher code
        System.out.println(this.couleurPrincipaleLogo); // afficher couleur
        System.out.println(this.couleurSecondaireleLogo);//afficher couleur secondaire
    }
    public static void main(String[] args){
        
        Compagnie maCompagnie1= new Compagnie();
        maCompagnie1.nom="zaza";
        maCompagnie1.code=15;
        maCompagnie1.couleurPrincipaleLogo="orange";
        maCompagnie1.couleurSecondaireleLogo="vert";
        maCompagnie1.afficher();
        
        Compagnie maCompagnie2= new Compagnie("zozo",15,"blue");//configurer Compagnie2 avec nom et code
        maCompagnie2.afficher(); //afficher compagnie2
        
        Compagnie maCompagnie3= new Compagnie("easyJune",15,"blue"); //configurer Compagnie3 affichant "easyJune" et code
        maCompagnie3.afficher(); //afficher compagnie3
        
        Compagnie maCompagnie4= new Compagnie("monChoix1",15,"blue"); //configurer Compagnie4 affiche le nom de mon choix avec le constructeur a un argument et code
        maCompagnie4.afficher(); //afficher compagnie4
        
        Compagnie maCompagnie5= new Compagnie();//configuer Compagnie5 avec le constructeur sans argument
        maCompagnie5.nom=("monChoix2"); //choix de nom de Compagnie5
        maCompagnie5.code=15;//afficher code Compagnie5
        maCompagnie5.couleurPrincipaleLogo=("blue");//afficher couleurprincipal Compagnie5
        maCompagnie5.afficher(); //afficherCompagnie5 
        
        Compagnie maCompagnie6= new Compagnie("cp6",15,"blue","red"); //configurer Compagnie6 affiche le nom de mon choix avec le constructeur a un argument et code
        maCompagnie6.afficher(); //afficher compagnie6
        Compagnie maCompagnie7= new Compagnie("AWA",15,"blue","red"); //configurer Compagnie7 affiche le nom de mon choix avec le constructeur a un argument et code
        maCompagnie7.afficher(); //afficher compagnie7
}
        public String getcode(){
            return code; }

        public String getnom(){
            return nom; }

        public String getca(){
            return ca; }

        public String getcouleurPrincipaleLogo(){
            return couleurPrincipaleLogo; }

        public String getcouleurSecondaireleLogo(){
            return couleurSecondaireleLogo; }

            public class AdressePostale{
                public String libellé;
                public String ville;
                public String codePostale; }
     
             public AdressePostale(){

             }
             public AdressePostale (String libelle, String ville, String codePostale){
                this.libelle=libelle;
                this.ville=ville;
                this.codePostale=codePostale;
            }
           AdressePostale ap= new AdressePostale ("2, rue Gabriel Faure","Jouy le Moutier","95280");
           Voyageur v1= new Voyageur ("Christophe Phillipe");
           System.out.println("Adresse de v1",+v1.getAdresse());
            }
        
                

}
