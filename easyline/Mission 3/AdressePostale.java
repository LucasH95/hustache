package fr.hautil.mission7;


public class AdressePostale {
/////création des variable numeroVoie, Ville , et Codepostale
 private String numeroVoie;
 private String Ville;
 private String CodePostale; 
 //////// definition du constructeur à trois arguments
    public  AdressePostale(String num, String v, String code ){
        this.numeroVoie=num;
        this.Ville=v;
        this.CodePostale=code;
    }
    ///accesseur Numerovoie , Ville , Codepostale
    public String getNumerovoie() {return numeroVoie;}
    public String getVille(){return Ville;}
    public String getCodepostale() {return CodePostale;}
    ///mutateur Numerovoie , Ville , Codepostale
    public void setNumeroVoie(String numeroVoie) {this.numeroVoie=numeroVoie;}
    public void setVille(String Ville) {this.Ville=Ville;}
    public void setCodepostale(String CodePostale) {this.CodePostale=CodePostale;}
    ///definiton de la methode toString  de la class AdressePostale
    public String toString(){return " "+"code postale :"+this.CodePostale+" "+"numero voie"+this.numeroVoie+" "+"ville"+getVille();}
    /* 
    public static void main(String[] args){
        adresse adresse1= new adresse();
        adresse1.adressePostal("67","cergy","95000");
        adresse1.setCodepostale("95000");
        adresse1.setVille("jouy");
        adresse1.setNumeroVoie("26");
        System.out.println(adresse1.numeroVoie+" "+adresse1.Ville+" "+adresse1.CodePostale);
     
    }
    */
}
