from math import *
def estPremier(n):
    if n==0 or n==1:
        return -1
    if n==2 or n==3 :
        return 1
    else:
        premier=True
        lim=sqrt(n)
        i=2
        while i<=lim and premier:
            if n%i==0:
                premier=False
            else:
                i=i+1
        if premier :
            return 1
        else:
            return -1

def PlusPetitPremier(m):
    if estPremier(m)==1:
        return m
    else:
        while estPremier(m)==-1:
            return m

def trouvePremier(i):
    k=0
    j=0
    while j!=i:
        k=PlusPetitPremier(k)+1
        j=j+1
        return i